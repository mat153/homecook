package com.mat.homecook.repository;


import com.mat.homecook.model.RecipePhotos;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RecipePhotosRepository extends JpaRepository<RecipePhotos,Integer> {

    RecipePhotos findByRecipeId(Integer idRecipe);
}
