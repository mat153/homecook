package com.mat.homecook.repository;

import com.mat.homecook.model.Recipe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface RecipeRepository extends JpaRepository<Recipe,Integer>{

    @Query(value = "select * from recipe where lower(recipe_name) like  %:keyword%",nativeQuery = true)
        List<Recipe> findByKeyword(@Param("keyword") String keyword);
        List<Recipe> findByUserId(Integer fileId);

    }

