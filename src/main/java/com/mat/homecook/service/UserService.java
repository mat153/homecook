package com.mat.homecook.service;

import com.mat.homecook.dto.UserRegistrationDto;
import org.springframework.security.core.userdetails.UserDetailsService;
import com.mat.homecook.model.User;

import java.util.List;

public interface UserService extends UserDetailsService{
	User save(UserRegistrationDto registrationDto);
	User findByEmail(String email);
	User findByNick(String nick);

	List<User> getFiles();
}
