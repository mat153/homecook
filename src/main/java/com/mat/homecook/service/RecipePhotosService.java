package com.mat.homecook.service;

import com.mat.homecook.model.Recipe;
import com.mat.homecook.model.RecipePhotos;
import com.mat.homecook.model.User;
import com.mat.homecook.repository.RecipePhotosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecipePhotosService {

    @Autowired
    private RecipePhotosRepository recipePhotosRepository;
    public List<RecipePhotos> getFiles() {
        return recipePhotosRepository.findAll();
    }

    public void saveRecipePhotos(RecipePhotos recipePhotos) {
        this.recipePhotosRepository.save(recipePhotos);
    }

}
