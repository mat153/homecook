package com.mat.homecook.service;

import com.mat.homecook.model.Recipe;
import com.mat.homecook.model.RecipePhotos;
import com.mat.homecook.model.User;
import com.mat.homecook.repository.RecipePhotosRepository;
import com.mat.homecook.repository.RecipeRepository;
import com.mat.homecook.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RecipeService {

    private final RecipeRepository recipeRepository;
    private final UserRepository userRepository;
    private final RecipePhotosRepository recipePhotosRepository;
    @Autowired
    public RecipeService(RecipeRepository recipeRepository, UserRepository userRepository, RecipePhotosRepository recipePhotosRepository) {
        this.recipeRepository = recipeRepository;
        this.userRepository = userRepository;
        this.recipePhotosRepository=recipePhotosRepository;
    }

    public List<Recipe> findByKeyword(String keyword) {
        return recipeRepository.findByKeyword(keyword.toLowerCase());
    }

    public Optional<Recipe> getFile(Integer fileId) { return recipeRepository.findById(fileId); }
    public List<Recipe> getFiles() {
        return recipeRepository.findAll();
    }

    public List<Recipe> getRecipesByUser(Integer fileId) {
        return recipeRepository.findByUserId(fileId);
    }

    public void saveRecipe(Recipe recipe) {
        this.recipeRepository.save(recipe);
    }

    public void saveRecipe(Recipe recipe, String nick){
        User user = userRepository.findByNick(nick);
        recipe.setUserid(user.getId());
        recipeRepository.save(recipe);
    }
    public void saveRecipe(Recipe recipe,String nick,Integer idRecipe){
        User user = userRepository.findByNick(nick);
        recipe.setUserid(user.getId());
        RecipePhotos recipePhotos= recipePhotosRepository.findByRecipeId(idRecipe);
        recipe.setPhotos(recipePhotos.getId());
        recipeRepository.save(recipe);
    }

    public List<Recipe> getRecipesUser(Integer fileId) {
        return recipeRepository.findByUserId(fileId);
    }



}

