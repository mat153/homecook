package com.mat.homecook.model;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.mat.homecook.repository.RecipePhotosRepository;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Entity
@Table(name = "Recipe")
public class Recipe {
        @Id
        @GeneratedValue(strategy= GenerationType.IDENTITY)
        private Integer id;

        @NotBlank(message = "title is mandatory")
        private String recipeName;

        @NotBlank(message = "description is mandatory")
        @Column(length = 10000)
        private String recipeDescription;

        @ManyToOne
        @JoinColumn(name = "photos",insertable = false,updatable = false)
        RecipePhotos recipePhotos;
        private Integer photos;

        @ManyToOne
        @JoinColumn(name="userid",insertable = false,updatable = false)
        User user;
        private Integer userid;

    public Recipe() {
    }

    public Recipe(Integer id, @NotBlank(message = "title is mandatory") String recipeName, @NotBlank(message = "description is mandatory") String recipeDescription, RecipePhotos recipePhotos, Integer photos, User user, Integer userid) {
        this.id = id;
        this.recipeName = recipeName;
        this.recipeDescription = recipeDescription;
        this.recipePhotos = recipePhotos;
        this.photos = photos;
        this.user = user;
        this.userid = userid;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @JsonBackReference
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getRecipeDescription() {
        return recipeDescription;
    }

    public void setRecipeDescription(String recipeDescription) {
        this.recipeDescription = recipeDescription;
    }

    public RecipePhotos getRecipePhotos() {
        return recipePhotos;
    }

    public void setRecipePhotos(RecipePhotos recipePhotos) {
        this.recipePhotos = recipePhotos;
    }

    @JsonBackReference
    public Integer getPhotos() { return photos; }

    public void setPhotos(Integer photos) {
        this.photos = photos;
    }

    public String getPhotosImagePath() {
        return "/user-photos/" + id + "/" + recipePhotos.getPhotoName();
    }



}
