package com.mat.homecook.model;


import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "recipe_photos")
public class RecipePhotos {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer id;
    private String photoName;

    @ManyToOne
    @JoinColumn(name = "recipeId",insertable = false,updatable = false)
    Recipe recipe;

    @JsonBackReference
    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    private Integer recipeId;

    public Integer getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(Integer recipeId) {
        this.recipeId = recipeId;
    }

    public RecipePhotos() {
    }

    public RecipePhotos(Integer id, String photoName) {
        this.id = id;
        this.photoName = photoName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

}
