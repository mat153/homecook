package com.mat.homecook.model;


import com.fasterxml.jackson.annotation.JsonManagedReference;

import java.util.Collection;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name =  "users")
public class User {

	@Id
	@GeneratedValue(strategy =  GenerationType.IDENTITY)
	private Integer id;

	@NotBlank
	@Column(unique = true,name = "nick")
	private String nick;
	private String name;

	@NotBlank
	@Column(unique = true,name = "email")
	private String email;

	@NotBlank
	private String password;


	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(
			name = "users_roles",
			joinColumns = @JoinColumn(
		            name = "user_id", referencedColumnName = "id"),
			inverseJoinColumns = @JoinColumn(
				            name = "role_id", referencedColumnName = "id"))

	private Collection<Role> roles;

	@OneToMany(mappedBy = "user")
	private List<Recipe> recipes;



	public User() {
	}

	public User(String nick, String name, String email, String password, Collection<Role> roles) {
		super();
		this.nick = nick;
		this.name = name;
		this.email = email;
		this.password = password;
		this.roles = roles;
	}

	public Integer getId() { return id; }
	public void setId(Integer id) { this.id = id; }
	public String getnick() { return nick; }
	public void setnick(String nick) { this.nick = nick; }
	public String getname() { return name; }
	public void setname(String name) { this.name = name; }
	public String getEmail() { return email; }
	public void setEmail(String email) { this.email = email; }
	public String getPassword() { return password; }
	public void setPassword(String password) { this.password = password; }
	public Collection<Role> getRoles() { return roles; }
	public void setRoles(Collection<Role> roles) { this.roles = roles; }

	@JsonManagedReference
	public List<Recipe> getRecipes() { return recipes; }

	public void setRecipes(List<Recipe> recipes) { this.recipes = recipes; }

}
