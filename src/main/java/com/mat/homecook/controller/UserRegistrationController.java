package com.mat.homecook.controller;

import com.mat.homecook.model.User;
import com.mat.homecook.dto.UserRegistrationDto;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mat.homecook.service.UserService;

import javax.validation.Valid;

@Controller
@RequestMapping("/registration")
public class UserRegistrationController {

	private UserService userService;
	public UserRegistrationController(UserService userService) {
		super();
		this.userService = userService;
	}


	@ModelAttribute("user")
    public UserRegistrationDto userRegistrationDto() {
        return new UserRegistrationDto();
    }

	@GetMapping
	public String showRegistrationForm() {
		return "registration";
	}

@PostMapping
public String registerUserAccount(@ModelAttribute("user") @Valid UserRegistrationDto userDto,
								  BindingResult result){

	User existing = userService.findByEmail(userDto.getEmail());
	User existingNick = userService.findByNick(userDto.getnick());
	if (existing != null){
		result.rejectValue("email", null, "Istnieje już konto zarejestrowane z tym adresem e-mail");
	}

	if (existingNick != null){
		result.rejectValue("nick", null, "Istnieje już konto zarejestrowane z tym nickiem");
	}

	if (result.hasErrors()){
		return "registration";
	}

	userService.save(userDto);
	return "redirect:/login";
}

}
