package com.mat.homecook.controller;
import com.mat.homecook.model.User;
import com.mat.homecook.repository.UserRepository;
import com.mat.homecook.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserServiceImpl userService;
    @Autowired
    private UserRepository userRepository;

    @GetMapping("/users")
    public String getAllUsers(Model model)
    {
        List<User> list=userService.getFiles();
        model.addAttribute("users",list);
        return "users";
    }




}
