package com.mat.homecook.controller;


import com.mat.homecook.config.FileUploadUtil;
import com.mat.homecook.model.Recipe;
import com.mat.homecook.model.RecipePhotos;
import com.mat.homecook.model.User;
import com.mat.homecook.repository.UserRepository;
import com.mat.homecook.service.RecipePhotosService;
import com.mat.homecook.service.RecipeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;


@Controller
public class RecipeController {

    @Autowired
    private RecipeService recipeService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RecipePhotosService recipePhotosService;

    @GetMapping("/recipes")
    public String getAllRecipes(Model model, String keyword) {
        if(keyword != null){
            model.addAttribute("recipes",recipeService.findByKeyword(keyword));
        }
        else {
            model.addAttribute("recipes",recipeService.getFiles());
        }
        return "recipes";
    }


    @GetMapping("/recipe/{fileId}")
    public String getRecipeById(@PathVariable Integer fileId, Model model) {
        Recipe recipe = recipeService.getFile(fileId).get();
        model.addAttribute("recipe", recipe);
        return "recipesById";
    }

    @GetMapping("/recipes/user/{fileId}")
    public String getRecipeByUser(@PathVariable Integer fileId, Model model) {
        List<Recipe> recipe = recipeService.getRecipesByUser(fileId);
        model.addAttribute("recipe", recipe);
        return "recipesUser";
    }

    @GetMapping("/recipes/user")
    public String getRecipesUser(Model model){

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByNick(currentPrincipalName);
        List<Recipe> recipe=recipeService.getRecipesUser(user.getId());
        model.addAttribute("recipe",recipe);

        return "recipesUsers";
    }

    @GetMapping("/newRecipe")
        public String showNewRecipeForm(Model model){
        Recipe recipe = new Recipe();
        model.addAttribute("recipe",recipe);
        return "new_recipe";
    }


    @PostMapping("/add-recipe")
    public String saveRecipe(@ModelAttribute("recipe") @Valid Recipe recipe,BindingResult result, @RequestParam("image") MultipartFile multipartFile)throws IOException {

        if(result.hasErrors()){
            return "new_recipe";
        }

        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());

        RecipePhotos recipePhotos = new RecipePhotos();
        recipePhotos.setPhotoName(fileName);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        recipeService.saveRecipe(recipe,currentPrincipalName);

        recipePhotos.setRecipeId(recipe.getId());
        recipePhotosService.saveRecipePhotos(recipePhotos);

        Integer idRecipe = recipePhotos.getRecipeId();

        recipeService.saveRecipe(recipe,currentPrincipalName,idRecipe);


        String uploadDir = "user-photos/" + recipe.getId();
        FileUploadUtil.saveFile(uploadDir, fileName, multipartFile);

        return "redirect:/";
    }




}
